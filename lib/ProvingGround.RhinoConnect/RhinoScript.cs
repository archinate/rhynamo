﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RhinoScript4;
using Rhino5x64;

namespace ProvingGround.RhinoConnect
{
  public class RhinoScript
  {
    #region "Public Members"
    private IRhino5x64Application _RhinoApp = null;
    private IRhinoScript _RhinoScript = null;
    #endregion

    public RhinoScript(RhinoApplication rhinoapp)
    {
      //widen scope
      _RhinoApp = rhinoapp.RhinoApp;
      _RhinoScript = rhinoapp.RhinoScript;
    }

    /// <summary>
    /// Sends a Rhino Command
    /// </summary>
    /// <param name="str"></param>
    public void SendCommand(string str)
    {
      try
      {
        _RhinoScript.Command(str);
      }
      catch { }
    }
  }
}
