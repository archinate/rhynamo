# Development for Rhynamo has been discontinued... #
Greetings Rhynamo users,

Please note that I have decided to stop development and maintenance on this project. Feel free to use and 
reference this code if you find it valuable in your work. If you are interested in a comprehensive set of 
interoperability tools for Rhino to Revit, please check out Proving Ground�s new commercial toolkit for 
workflows - [Conveyor](http://apps.provingground.io/conveyor).

Thank you for your interest - I hope the code can provide some value even though I will no longer be working 
on this project.

Best regards,

Nate Miller

CEO, [Proving Ground](http://provingground.io)


# Welcome to the Rhynamo Project! #
![InstallerLogo.JPG](https://bitbucket.org/repo/Apdne7/images/3365568768-InstallerLogo.JPG)

### What is Rhynamo? ###

Rhynamo is an open source plug-in authored by Nathan Miller (Formally of CASE, now founder of Proving Ground).  Rhynamo exposes new visual nodes for reading and writing Rhino 3dm files.  These tools expose new workflow opportunities between common design and production environments used in architectural design.  We want Rhynamo to be helpful to your workflow, boost your self-esteem, and make you look cool in social situations.

### Who maintains Rhynamo? ###

Rhynamo is maintained by Nathan Miller (http:://provingground.io) in a bunker somewhere near the center of the United States...

* Are you interested in learning **Dynamo** or **Rhynamo**?  
* Are you looking to implement **interoperability** solutions that can benefit you and your business?  

### How do I get started? ###

You will need Dynamo (http://DynamoBIM.org) to use Rhynamo.  In most cases, Rhino 5.0 is not required to read/write files.  Rhino is required for "live" Rhino connections

There are currently two options for getting Rhynamo...

* Visit the Dynamo "Package Manager" - http://dynamopackages.com/
* Build Rhynamo from the source (Visual Studio 2013 C# Project)

### What comes with Rhynamo? ###

Rhynamo ships will several important libraries...

* Rhynamo.dll - A "zero touch" library containing nodes for Dynamo. 
* McNeel's OpenNURBS - https://github.com/mcneel/rhinocommon/wiki/Rhino3dmIO-Toolkit-(OpenNURBS-build)
* CASE_RhinoConnection.dll - A library for creating live connections with Rhino

### How can I contribute? ###

If you have come across bugs or have wish list items, [submit them to the Issues section of the Rhynamo repo.](https://bitbucket.org/archinate/rhynamo/issues?status=new&status=open)

If you have built some cool stuff for Rhynamo and would like to share it back with the official project, you can follow these steps...

*  [Fork the Rhynamo repo.](https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo,+Compare+Code,+and+Create+a+Pull+Request)
*  Make cool stuff.
*  Submit a [Pull Request](https://confluence.atlassian.com/display/BITBUCKET/Work+with+pull+requestst)

### Thanks! ###
Rhynamo wouldn't be the same without these folks...  We'd like to say thanks to....

* The Dynamo and Autodesk teams for their continued support and help... and thanks for Dynamo!
*  McNeel for providing an amazing open source geometry library:  OpenNURBS!
* Over 300 people who signed up to be Rhynamo testers this fall.  Your feedback was invaluable!
* Thanks to Brian Ringley (@brianringley) for the name idea... sorry, no royalties on free open source stuff :)

### The MIT License (MIT) ###

Rhynamo is an open source project under the [MIT](http://opensource.org/licenses/MIT) license

Copyright (c) 2014 Nathan Miller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.