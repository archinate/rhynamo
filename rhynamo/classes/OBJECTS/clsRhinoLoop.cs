﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino.FileIO;
using Rhino.Geometry;
using Rhino.Geometry.Collections;
using Rhino.Collections;

namespace Rhynamo.classes
{
  class clsRhinoLoop
  {
    private bool _issurface;

    public bool IsUntrimmedSurface
    {
      get
      {
        return _issurface;
      }
    }

    public List<Rhino.Geometry.Curve> LoopCurves;

    public clsRhinoLoop(bool IsSurface)
    {
      LoopCurves = new List<Rhino.Geometry.Curve>();
      _issurface = IsSurface;
    }
  }
}
