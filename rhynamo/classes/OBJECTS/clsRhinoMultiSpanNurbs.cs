﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Autodesk.DesignScript.Runtime;
using Autodesk.DesignScript.Geometry;

using Rhino.FileIO;
using Rhino.Geometry;
using Rhino.Geometry.Collections;
using Rhino.Collections;

namespace Rhynamo.classes
{
  class clsRhinoMultiSpanNurbs
  {
    public List<Autodesk.DesignScript.Geometry.NurbsCurve> MultiSpanNurbsList;
    public clsRhinoMultiSpanNurbs(List<Autodesk.DesignScript.Geometry.NurbsCurve> nurbs)
    {
      //widen scope
      MultiSpanNurbsList = nurbs;
    }
  }
}
