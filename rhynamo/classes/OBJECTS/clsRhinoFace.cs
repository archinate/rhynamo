﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Rhino.FileIO;
using Rhino.Geometry;
using Rhino.Geometry.Collections;
using Rhino.Collections;


namespace Rhynamo.classes
{
  class clsRhinoFace
  {
    private NurbsSurface _rhsurface;
    private clsRhinoLoop _rhloop;
    private bool _issurface;

    /// <summary>
    /// the face surface
    /// </summary>
    public NurbsSurface SurfaceFace
    {
      get
      {
        return _rhsurface;
      }
    }

    /// <summary>
    /// Loop curves
    /// </summary>
    public clsRhinoLoop EdgeLoop
    {
      get
      {
        return _rhloop;
      }
    }

    /// <summary>
    /// Is the face untrimmed?
    /// </summary>
    public bool IsUntrimmedSurface
    {
      get
      {
        return _issurface;
      }
    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="surface">The untrimmed NurbsSurface</param>
    /// <param name="loop">The Rhino loop</param>
    /// <param name="IsSurface">Is the face and untrimmed surface?</param>
    public clsRhinoFace(NurbsSurface surface, clsRhinoLoop loop, bool IsSurface)
    {
      _rhsurface = surface;
      _rhloop = loop;
      _issurface = IsSurface;
    }
  }
}
