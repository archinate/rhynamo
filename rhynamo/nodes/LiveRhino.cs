﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ProvingGround.RhinoConnect;

namespace Interoperability
{
  /// <summary>
  /// Send Rhino Commands
  /// </summary>
  public static class LiveRhino
  {
    #region "Rhino COM Components"

    /// <summary>
    /// Starts an instance of the Rhino Application
    /// </summary>
    /// <param name="StartRhino">Start Rhino</param>
    /// <returns name = "RhinoApplication">The Rhino Application object</returns>
    /// <search>case,rhino,3dm,rhynamo,live</search>
    public static RhinoApplication RhinoApp(bool StartRhino) // bool IsVisible)
    {
      try
      {
        if (StartRhino == true)
        {
          // CASE Rhino Connection Class
          ProvingGround.RhinoConnect.RhinoApplication m_RhinoCOM = new ProvingGround.RhinoConnect.RhinoApplication("Rhino5x64.Application", true);

          return m_RhinoCOM;
        }
        else { return null; }
      }
      catch { return null; }
    }

    /// <summary>
    /// Sends a Command to Rhino
    /// </summary>
    /// <param name="RhinoApp">Rhino Application</param>
    /// <param name="Command">Command</param>
    /// <param name="SendCommand">Boolean Toggle</param>
    /// <search>case,rhino,3dm,rhynamo,live,command</search>
    public static string RhinoCommand(RhinoApplication RhinoApp, string Command, bool SendCommand)
    {
      try
      {
        if (SendCommand == true)
        {

          ProvingGround.RhinoConnect.RhinoScript m_RhinoScript = new ProvingGround.RhinoConnect.RhinoScript(RhinoApp);
          m_RhinoScript.SendCommand(Command);

          return "Command Sent Success";
        }
        else { return "Set to true to send commands."; }

      }
      catch (Exception ex) { return ex.ToString(); }
    }

    ///// <summary>
    ///// RhinoScriptPoint Commands
    ///// </summary>
    ///// <param name="x">X Coordinate</param>
    ///// <param name="y">Y Coordinate</param>
    ///// <param name="z">Z Coordinate</param>
    ///////// <returns name = "RhinoScript Command">RhinoScript</returns>
    //////public static string Points_Cmd(double x, double y, double z)
    //////{
    //////  try
    //////  {
    //////    string cmd = "Point " + x.ToString() + "," + y.ToString() + "," + z.ToString();
    //////    return cmd;
    //////  }
    //////  catch { return null; }
    //////}

    #endregion
  }
}
